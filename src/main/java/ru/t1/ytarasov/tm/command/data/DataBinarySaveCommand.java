package ru.t1.ytarasov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.Domain;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-binary-save";

    @NotNull
    public static final String DESCRIPTION = "Save to binary file";

    @Override
    public void execute() throws AbstractException, IOException {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
