package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    void login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId() throws AbstractException;

    @Nullable
    User getUser() throws AbstractException;

    void checkRoles(@Nullable Role[] roles) throws AbstractException;
}
