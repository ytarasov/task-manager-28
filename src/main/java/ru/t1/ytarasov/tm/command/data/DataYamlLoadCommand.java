package ru.t1.ytarasov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.Domain;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataYamlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-yaml-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from yaml file.";

    @SneakyThrows
    @Override
    public void execute() throws AbstractException, IOException, ClassNotFoundException {
        System.out.println("[YAML LOAD]");
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
        @NotNull final String yaml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
