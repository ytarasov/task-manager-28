package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator comparator);

    int getSize();

    @Nullable
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id) throws AbstractException;

    @Nullable
    M findOneByIndex(int index) throws AbstractException;

    void clear();

    @Nullable
    M remove(M model) throws AbstractException;

    @Nullable
    M removeById(String id) throws AbstractException;

    @Nullable
    M removeByIndex(int index) throws AbstractException;

}
