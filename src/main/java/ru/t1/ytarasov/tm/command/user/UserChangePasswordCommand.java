package ru.t1.ytarasov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "change-password";

    @NotNull
    public static final String DESCRIPTION = "Change password";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE PASSWORD]");
        @Nullable final String userId = getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        System.out.println("ENTER NEW PASSWORD");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        getUserService().setPassword(userId, newPassword);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role [] getRoles() {
        return Role.values();
    }

}
